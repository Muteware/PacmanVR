﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Caminar : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField] private GameObject cabeza;
    [SerializeField] private GameObject textoPuntaje;
    [SerializeField] private GameObject canvasPartida;
    [SerializeField] private GameObject canvasFinal;
    private int puntaje;
    private Text puntajeString;
    private bool activarContador;
    private float tiempoCaza;
    private float temporizadorCaza;
    private bool puedeMoverse;
    private float tiempoPartida;
    private bool terminoPartida;
    private float velocidad;

    private bool puedoTerminar;
    private float temporizadorTerminar;

    
    #endregion

    #region PROPIEDADES
    public GameObject Cabeza
    {
        get
        {
            return cabeza;
        }

        set
        {
            cabeza = value;
        }
    }

    public GameObject TextoPuntaje
    {
        get
        {
            return textoPuntaje;
        }

        set
        {
            textoPuntaje = value;
        }
    }

    public int Puntaje
    {
        get
        {
            return puntaje;
        }

        set
        {
            puntaje = value;
        }
    }

    public Text PuntajeString
    {
        get
        {
            return puntajeString;
        }

        set
        {
            puntajeString = value;
        }
    }

    public bool ActivarContador
    {
        get
        {
            return activarContador;
        }

        set
        {
            activarContador = value;
        }
    }

    public float TiempoCaza
    {
        get
        {
            return tiempoCaza;
        }

        set
        {
            tiempoCaza = value;
        }
    }

    public float TemporizadorCaza
    {
        get
        {
            return temporizadorCaza;
        }

        set
        {
            temporizadorCaza = value;
        }
    }

    public bool PuedeMoverse
    {
        get
        {
            return puedeMoverse;
        }

        set
        {
            puedeMoverse = value;
        }
    }

    public GameObject CanvasFinal
    {
        get
        {
            return canvasFinal;
        }

        set
        {
            canvasFinal = value;
        }
    }

    public GameObject CanvasPartida
    {
        get
        {
            return canvasPartida;
        }

        set
        {
            canvasPartida = value;
        }
    }

    public float TiempoPartida
    {
        get
        {
            return tiempoPartida;
        }

        set
        {
            tiempoPartida = value;
        }
    }

    public bool TerminoPartida1
    {
        get
        {
            return terminoPartida;
        }

        set
        {
            terminoPartida = value;
        }
    }

    public float Velocidad
    {
        get
        {
            return velocidad;
        }

        set
        {
            velocidad = value;
        }
    }

    public bool PuedoTerminar
    {
        get
        {
            return puedoTerminar;
        }

        set
        {
            puedoTerminar = value;
        }
    }

    public float TemporizadorTerminar
    {
        get
        {
            return temporizadorTerminar;
        }

        set
        {
            temporizadorTerminar = value;
        }
    }
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Start ()
    {
        this.PuedoTerminar = false;
        this.TemporizadorTerminar = 10f;
        this.TiempoPartida = 0;
        this.PuntajeString = this.TextoPuntaje.GetComponent<Text>();
        this.Puntaje = 0;
        this.PuntajeString.text = "Score: " + this.Puntaje;
        this.TiempoCaza = 10f;
        this.ActivarContador = false;
        this.PuedeMoverse = true;
        this.terminoPartida = false;
        this.Velocidad = 0.040f;
	}

    // Update is called once per frame
    void Update()
    {
        if (this.PuedoTerminar)
        { 
            this.TemporizadorTerminar -= Time.deltaTime;
            GameObject.Find("TiempoTerminar").GetComponent<Text>().text = this.TemporizadorTerminar.ToString("0");
        }

        if (this.TemporizadorTerminar <= 0)
            SceneManager.LoadScene("Menu Principal");

        if(!this.TerminoPartida1)
            this.TiempoPartida += Time.deltaTime;

        if(this.puedeMoverse)
            this.Moverse();

        if (this.ActivarContador)
        {
            if (this.TemporizadorCaza > 0)
                this.TemporizadorCaza -= Time.deltaTime;
            else
            {
                this.CambiarEstado();
                this.ActivarContador = false;
            }
        }

        GameObject[] listaEsferasChicas = GameObject.FindGameObjectsWithTag("EsferaChica");
        if (listaEsferasChicas.Length <= 0)
        {
            this.TerminoPartida();
            this.ActivarNuevaGUIGanaste();
        }

    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "EsferaChica")
        {
            this.SumarPuntos(100);
            Destroy(collider.gameObject);
        }

        if (collider.gameObject.tag == "EsferaGrande")
        {
            this.SumarPuntos(500);
            this.CambiarEstado();
            Destroy(collider.gameObject);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Fantasma")
        {
            if (!collision.gameObject.GetComponent<Fantasma>().PuedeCazar)
                Destroy(collision.gameObject);
            else
            {
                this.TerminoPartida();

                this.ActivarNuevaGUIPerdida();
            }
        }

        if (collision.gameObject.name == "Cube")
        {
            this.TerminoPartida();

            this.ActivarNuevaGUIPerdida();
        }
    }
    #endregion

    #region METODOS
    private void SumarPuntos(int puntaje)
    {
        this.Puntaje += puntaje;
        this.PuntajeString.text = "Score: " + this.Puntaje;
    }

    private void CambiarEstado()
    {
        this.ActivarContador = !this.ActivarContador;
        this.TemporizadorCaza = this.TiempoCaza;

        GameObject[] listaFantasmas = GameObject.FindGameObjectsWithTag("Fantasma");

        foreach (GameObject fantasma in listaFantasmas)
        {
            fantasma.GetComponent<Fantasma>().PuedeCazar = !fantasma.GetComponent<Fantasma>().PuedeCazar;
        }
    }

    private void Moverse()
    {
        this.transform.Translate(this.Cabeza.transform.forward * this.Velocidad, this.transform);
        this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, 1.75f, this.gameObject.transform.position.z);
    }

    private void ActivarNuevaGUIPerdida()
    {
        this.CanvasPartida.SetActive(false);
        this.CanvasFinal.SetActive(true);
        GameObject.Find("Resultado").GetComponent<Text>().text = "You Lose ! !";
        GameObject.Find("Puntaje").GetComponent<Text>().text = "Score: " + this.Puntaje;
        GameObject.Find("Tiempo").GetComponent<Text>().text = "You last: " + this.TiempoPartida + "s";
        GameObject.Find("Mensaje").GetComponent<Text>().text = "Going to Menu in... ";
        this.PuedoTerminar = true;

    }

    private void ActivarNuevaGUIGanaste()
    {
        this.CanvasPartida.SetActive(false);
        this.CanvasFinal.SetActive(true);
        GameObject.Find("Resultado").GetComponent<Text>().text = "Conglaturation !!! - A winner is you";
        GameObject.Find("Puntaje").GetComponent<Text>().text = "Score: " + this.Puntaje;
        GameObject.Find("Tiempo").GetComponent<Text>().text = "Time: " + this.TiempoPartida + "s";
        GameObject.Find("Mensaje").GetComponent<Text>().text = "You have completed a great game.\nand prooved the justice of our culture.\nnow go and rest our heroes !\n Going to Menu in...";
        this.PuedoTerminar = true;

    }

    private void TerminoPartida()
    {
        this.PuedeMoverse = false;
        this.TerminoPartida1 = true;
        GameObject[] listaFantasmas = GameObject.FindGameObjectsWithTag("Fantasma");
        if(PlayerPrefs.GetFloat("Puntaje") < this.Puntaje)
            PlayerPrefs.SetFloat("Puntaje", this.Puntaje);
        foreach (GameObject fantasma in listaFantasmas)
        {
            fantasma.GetComponent<Fantasma>().Apagarse();
        }
    }
    #endregion

}
