﻿using UnityEngine;
using System.Collections;
using System;

public class Fantasma : MonoBehaviour
{
    #region ATRIBUTOS
    private NavMeshAgent agente;
    private Transform jugador;
    private GameObject[] listaEscape;
    private bool puedeCazar;
    public bool puedeMoverse;
    [SerializeField] private Material materialAmarillo;
    [SerializeField] private Material materialAzul;
    #endregion

    #region PROPIEDADES

    public NavMeshAgent Agente
    {
        get
        {
            return agente;
        }

        set
        {
            agente = value;
        }
    }

    public Transform Jugador
    {
        get
        {
            return jugador;
        }

        set
        {
            jugador = value;
        }
    }

    public bool PuedeCazar
    {
        get
        {
            return puedeCazar;
        }

        set
        {
            puedeCazar = value;
        }
    }

    public GameObject[] ListaEscape
    {
        get
        {
            return listaEscape;
        }

        set
        {
            listaEscape = value;
        }
    }

    public bool PuedeMoverse
    {
        get
        {
            return puedeMoverse;
        }

        set
        {
            puedeMoverse = value;
        }
    }

    public Material MaterialAmarillo
    {
        get
        {
            return materialAmarillo;
        }

        set
        {
            materialAmarillo = value;
        }
    }

    public Material MaterialAzul
    {
        get
        {
            return materialAzul;
        }

        set
        {
            materialAzul = value;
        }
    }
    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Start ()
    {
        this.Agente = this.GetComponent<NavMeshAgent>();
        this.Jugador = GameObject.Find("CardboardMain").transform;
        this.PuedeCazar = true;
        this.ListaEscape = GameObject.FindGameObjectsWithTag("Posicion Escape");
        this.PuedeMoverse = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(this.PuedeMoverse)
        {
            if (this.PuedeCazar)
            { 
                this.PerseguirJugador();
                this.SerAmarillo();
            }
            else
            {
                this.EscaparJugador();
                this.SerAzul();
            }                
        }
	}
    #endregion

    #region METODOS
    private void PerseguirJugador()
    {
        this.Agente.destination = this.Jugador.position;
    }

    private void EscaparJugador()
    {
        GameObject puntoMasLejano = new GameObject();
        float distancia = 0;

        foreach (GameObject puntoEscape in this.ListaEscape)
        {
            float nuevaDistancia = Vector3.Distance(puntoEscape.transform.position, this.Jugador.position);

            if (nuevaDistancia > distancia)
            {
                puntoMasLejano = puntoEscape;
                distancia = nuevaDistancia;
            }
        }

        this.Agente.destination = puntoMasLejano.transform.position;
    }

    public void Apagarse()
    {
        this.PuedeMoverse = false;
        this.GetComponent<NavMeshAgent>().enabled = false;
    }

    private void SerAmarillo()
    {
        this.gameObject.GetComponent<MeshRenderer>().material = this.MaterialAmarillo;
    }

    private void SerAzul()
    {
        this.gameObject.GetComponent<MeshRenderer>().material = this.MaterialAzul;
    }
    #endregion
}
