﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuPrincipal : MonoBehaviour
{
    #region ATRIBUTOS
    [SerializeField]
    private GameObject puntajeMasAlto;
    [SerializeField]
    private GameObject publicidad;
    [SerializeField]
    private GameObject contador;

    private bool puedoRestarTiempo;
    private float tiempoHastaComenzar;


    #endregion

    #region PROPIEDADES
    public GameObject PuntajeMasAlto
    {
        get
        {
            return puntajeMasAlto;
        }

        set
        {
            puntajeMasAlto = value;
        }
    }

    public GameObject Publicidad
    {
        get
        {
            return publicidad;
        }

        set
        {
            publicidad = value;
        }
    }

    public bool PuedoRestarTiempo
    {
        get
        {
            return puedoRestarTiempo;
        }

        set
        {
            puedoRestarTiempo = value;
        }
    }

    public float TiempoHastaComenzar
    {
        get
        {
            return tiempoHastaComenzar;
        }

        set
        {
            tiempoHastaComenzar = value;
        }
    }

    public GameObject Contador
    {
        get
        {
            return contador;
        }

        set
        {
            contador = value;
        }
    }

    #endregion

    #region METODOS UNITY
    // Use this for initialization
    void Start ()
    {
        this.PuedoRestarTiempo = false;
        this.TiempoHastaComenzar = 5;
        this.PuntajeMasAlto.GetComponent<Text>().text = PlayerPrefs.GetFloat("Puntaje").ToString();
        this.Contador.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        this.Contador.GetComponent<Text>().text = this.TiempoHastaComenzar.ToString("0");

        if (this.PuedoRestarTiempo)
            this.TiempoHastaComenzar -= Time.deltaTime;

        if (this.TiempoHastaComenzar <= 0)
            SceneManager.LoadScene("Nivel 1");
	}
    #endregion

    #region METODOS
    public void MoverHaciaAdelante(GameObject unObjeto)
    {
        unObjeto.transform.Translate(0f, 0f, -1f);
    }

    public void MoverHaciaAdelanteComenzar(GameObject unObjeto)
    {
        unObjeto.transform.Translate(0f, 0f, -1f);
        this.Contador.SetActive(true);
        this.TiempoHastaComenzar = 5;
        this.PuedoRestarTiempo = true;
    }

    public void MoverHaciaAtras(GameObject unObjeto)
    {
        unObjeto.transform.Translate(0f, 0f, 1f);
        this.PuedoRestarTiempo = false;
        this.Contador.SetActive(false);
    }

    public void Looog()
    {
        Debug.Log("asd");
    }
    #endregion
}
